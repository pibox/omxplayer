# ---------------------------------------------------------------
# Build omxplayer
# ---------------------------------------------------------------
root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

$(OMX_T)-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(SD)" = "" ] || [ ! -d $(SD) ]; then \
		$(MSG11) "Can't find staging tree.  Try setting SD= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(OMX_T)-get $(OMX_T)-get: 
	@mkdir -p $(BLDDIR) 
	@if [ ! -d $(OMX_SRCDIR) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving OMX source" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(BLDDIR) && git clone $(OMX_URL) $(OMX_VERSION); \
		cd $(BLDDIR)/$(OMX_VERSION) && git checkout $(OMX_COMMIT); \
	else \
		$(MSG3) "OMX source is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(OMX_T)-get-patch $(OMX_T)-get-patch: .$(OMX_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(OMX_T)-unpack $(OMX_T)-unpack: .$(OMX_T)-get-patch
	@if [ ! -d $(OMX_SRCDIR) ]; then \
		if [ -d $(OMX_ARCDIR)/$(KSRC) ]; then \
			$(MSG3) "Copying omxplayer archive to build dir" $(EMSG); \
			mkdir -p $(OMX_SRCDIR); \
			rsync -a $(OMX_ARCDIR)/$(KSRC)/ $(OMX_SRCDIR)/; \
		else \
			$(MSG11) "omxplayer source archive is missing:" $(EMSG); \
			$(MSG11) "$(OMX_ARCDIR)/$(KSRC) not found " $(EMSG); \
			exit 1; \
		fi; \
	fi
	@touch .$(subst .,,$@)

# Apply patches
.$(OMX_T)-patch $(OMX_T)-patch: .$(OMX_T)-unpack
	@cp $(DIR_OMX)/Makefile $(OMX_SRCDIR)/
	@cp $(DIR_OMX)/Makefile.include $(OMX_SRCDIR)/
	@sed -i 's%\[OMXSTAGING\]%$(OMX_STAGING)%g' $(OMX_SRCDIR)/Makefile.include
	@sed -i 's%\[XI\]%$(XCC_PREFIXDIR)%g' $(OMX_SRCDIR)/Makefile.include
	@sed -i 's%\[XCC_PREFIX\]%$(XCC_PREFIX)%g' $(OMX_SRCDIR)/Makefile.include
	@echo cp $(DIR_OMX)/Makefile.ffmpeg $(OMX_SRCDIR)/
	@touch .$(subst .,,$@)

.$(OMX_T)-init $(OMX_T)-init: 
	@make $(OMX_T)-verify
	@make .$(OMX_T)-patch
	@touch .$(subst .,,$@)

# Build the package
$(OMX_T): .$(OMX_T)

.$(OMX_T): .$(OMX_T)-init 
	@$(MSG) "================================================================"
	@$(MSG2) "Building OMX" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(OMX_SRCDIR) && echo make ffmpeg
	@cd $(OMX_SRCDIR) && make -j $(JOBS)
	@touch .$(subst .,,$@)

$(OMX_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "OMX Build Files ($(OMX_SRCDIR)/builders/make/output)" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(OMX_SRCDIR)/omxplayer.bin

# Package it as an opkg 
opkg pkg $(OMX_T)-pkg: .$(OMX_T) root-verify opkg-verify
	@mkdir -p $(OMX_BLDDIR)/opkg/omxplayer/CONTROL
	@mkdir -p $(OMX_BLDDIR)/opkg/omxplayer/usr/share/fonts/pibox
	@mkdir -p $(OMX_BLDDIR)/opkg/omxplayer/etc/ld.so.conf.d
	@cd $(OMX_SRCDIR) && make DIST=$(OMX_BLDDIR)/opkg/omxplayer/ dist
	@cp $(OMX_SRCDIR)/fonts/FreeSans.ttf $(OMX_BLDDIR)/opkg/omxplayer/usr/share/fonts/pibox
	@cp $(DIR_OMX)/omxplayer.conf $(OMX_BLDDIR)/opkg/omxplayer/etc/ld.so.conf.d
	@cp $(SRCDIR)/opkg/control $(OMX_BLDDIR)/opkg/omxplayer/CONTROL/control
	@cp $(SRCDIR)/opkg/postinst $(OMX_BLDDIR)/opkg/omxplayer/CONTROL/postinst
	@cp $(SRCDIR)/opkg/prerm $(OMX_BLDDIR)/opkg/omxplayer/CONTROL/prerm
	@cp $(SRCDIR)/opkg/debian-binary $(OMX_BLDDIR)/opkg/omxplayer/CONTROL/debian-binary
	@sed -i 's%\[VERSION\]%'`cat $(TOPDIR)/version.txt`'%' $(OMX_BLDDIR)/opkg/omxplayer/CONTROL/control
	@chmod +x $(OMX_BLDDIR)/opkg/omxplayer/CONTROL/postinst
	@chmod +x $(OMX_BLDDIR)/opkg/omxplayer/CONTROL/prerm
	@sudo chown root.root $(OMX_BLDDIR)/opkg/omxplayer
	@cd $(OMX_BLDDIR)/opkg/ && $(OPKG_DIR)/opkg-build -o root -g root -O omxplayer
	@mkdir -p $(PKGDIR)/
	@cp $(OMX_BLDDIR)/opkg/*.opk $(PKGDIR)/

# Clean the packaging
pkg-clean $(OMX_T)-pkg-clean:
	@rm -rf $(PKGDIR) $(OMX_BLDDIR)/*

# Clean out a cross compiler build but not the CT-NG package build.
$(OMX_T)-clean:
	@cd $(OMX_SRCDIR) && make PLATFORM=pibox clean
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm *; fi
	@rm -f .$(OMX_T) 

# Clean out everything associated with OMX
$(OMX_T)-clobber: 
	@rm -rf $(PKGDIR) $(OMX_SRCDIR) $(OMX_ARCDIR) $(OMX_BLDDIR)
	@rm -f .$(OMX_T)-init .$(OMX_T)-patch .$(OMX_T)-unpack .$(OMX_T)-get .$(OMX_T)-get-patch
	@rm -f .$(OMX_T) 

